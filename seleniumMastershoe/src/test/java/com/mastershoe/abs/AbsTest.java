package com.mastershoe.abs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.mastershoe.javaee.propiedades.DatosSistema;

public class AbsTest {

	private static final Logger LOGGER = Logger.getLogger(AbsTest.class);
	private static final String EXTENSION_IMAGEN = ".png";
	private static final String WEB_URL = "https://www.falabella.com/falabella-cl/";

	/**
	 * Capturar Pantalla
	 * 
	 * @param driver
	 * @param nombreImagen
	 * @param carpeta
	 * @throws IOException
	 */

	protected WebDriver driver;
	protected String baseUrl;
	protected String parameterUrl;
	protected boolean acceptNextAlert = true;
	protected StringBuffer verificationErrors = new StringBuffer();
	

	@Before
	public void setUp() throws Exception {
		LOGGER.info("INICIANDO @BEFORE setUp():" + this.getClass().getName());

		String operationSystem = System.getProperty("os.name").toLowerCase();
		if (operationSystem.indexOf("win") >= 0) {
			LOGGER.info("setUp():ES WINDOWS");
			System.setProperty("webdriver.chrome.driver", "./src/main/resources/Runners/chromedriver.exe");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");

			driver = new ChromeDriver(options);

			LOGGER.info("driver Chrome:" + driver);
		} else if (operationSystem.indexOf("nix") >= 0 || operationSystem.indexOf("nux") >= 0
				|| operationSystem.indexOf("aix") > 0) {
			LOGGER.info("setUp():ES LINUX");
			System.setProperty("phantomjs.binary.path", "./src/main/resources/Runners/phantomjs_Linux64");
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setJavascriptEnabled(true);
			caps.setCapability("takesScreenshot", true);
			caps.setCapability("phantomjs.cli.args",
					new String[] { "--web-security=false", "--ignore-ssl-errors=true" });
			driver = new PhantomJSDriver(caps);

			baseUrl = DatosSistema.getDatoProperties("url");
		} else {
			throw new Exception("Error al setear el Sistema operativo");
		}
	}

	@After
	public void tearDown() throws Exception {

		LOGGER.info("INICIANDO @AFTER tearDown():" + this.getClass().getName());
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			LOGGER.info("*********************************************");
			LOGGER.error(this.getClass().getName() + ",ERRORS:" + verificationErrorString);
			LOGGER.info("*********************************************");
			fail(verificationErrorString);
		}

	}

	protected String getValueFromInput(WebElement element) {
		return element.getAttribute("value");
	}

	protected boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	protected boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	protected String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public static void waitSleep(int l) {
		for (int i = 0; i < l; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected void capturarPantalla(WebDriver driver, String nombreImagen, String carpeta) throws IOException {
		LOGGER.info("Captura de pantalla URL: " + driver.getCurrentUrl() + " - Carpeta: ./screenshots/" + carpeta
				+ " - Nombre imagen: " + nombreImagen + EXTENSION_IMAGEN);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./screenshots/" + carpeta + "/" + nombreImagen + EXTENSION_IMAGEN));

	}

	// Compara texto primer texto corresponde a UTF
	protected void compara(String texto1, String texto2) {
		try {
			// String textoUTF = cambiaTexto(texto1);
			// assertEquals(textoUTF, texto2);
			assertEquals(texto1, texto2);

		} catch (Exception e) {
			LOGGER.error("compara(), exception,", e);
		}

	}

	/**
	 * EliminarCarpeta
	 * 
	 * @param carpeta
	 * @throws IOException
	 */
	public static void eliminarCarpeta(String carpeta) throws IOException {
		LOGGER.info("Eliminar archivos de la carpeta: ./screenshots/" + carpeta);
		FileUtils.deleteDirectory(new File("./screenshots/" + carpeta));

	}

}
