package com.mastershoe.javaee.propiedades;

import org.apache.log4j.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatosSistema {
	private static final String DESARROLLO = "desarrollo";
	private static final String NODE_ENV = "NODE_ENV";
	public static final String ARCHIVO_PROPIEDADES = "mastershoe.properties";
	public static final String ARCHIVO_PROPIEDADES_QA = "mastershoeQA.properties";
	private static Properties properties;
	public static final int TIME_OUT;
	public static final String URL;

	private static final Logger LOGGER = Logger.getLogger(DatosSistema.class);

	static {

		InputStream input;

		try {
			LOGGER.info("Obtener Propiedades - init");
			properties = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			String env = System.getenv(NODE_ENV);
			if (env == null || DESARROLLO.equals(env)) {
				input = loader.getResourceAsStream(ARCHIVO_PROPIEDADES);
			} else {
				input = loader.getResourceAsStream(ARCHIVO_PROPIEDADES_QA);
			}

			properties.load(input);
			TIME_OUT = Integer.parseInt(properties.getProperty("timeOut"));

			URL = properties.getProperty("url");
			LOGGER.info("URL: " + URL);

		} catch (FileNotFoundException e) {
			throw new RuntimeException("Excepcion - Archivo " + ARCHIVO_PROPIEDADES + " no encontrado.", e);
		} catch (IOException e) {
			throw new RuntimeException("Excepcion - Archivo " + ARCHIVO_PROPIEDADES + " no pudo ser cargado.",
					e);
		}
	}

	public static String getDatoProperties(String value) {
		return properties.getProperty(value);
	}

}
