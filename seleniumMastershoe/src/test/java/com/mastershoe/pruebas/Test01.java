package com.mastershoe.pruebas;

import org.apache.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import com.mastershoe.abs.AbsTest;
import com.mastershoe.javaee.propiedades.DatosSistema;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class Test01 extends AbsTest {

	private static final Logger LOGGER = Logger.getLogger(Test01.class);
	
	static By categoria = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Maquillaje Más Verde'])[1]/following::div[4]");
	static By tecnologia = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telefonía'])[1]/following::div[4]");
	static By consola = By.linkText("Consolas");
	static By despleMarca = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Marca'])[1]/following::i[1]");
	static By marca = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='bethesda'])[1]/following::span[2]");
	static By detProd = By.xpath("//*[@id=\"testId-Pod-action-6091777\"]");
	static By agregarProd = By.xpath("//*[@id=\"fbra_browseMainProduct\"]/div/div/div[2]/div/div[7]/div/div/button[2]");

	@Test
	public void Test01Flujo() throws Exception {
		String urlLoggin = DatosSistema.getDatoProperties("url");
		
		/* INGRESO DE APLICATIVO */
		LOGGER.info(" VALIDACION INGRESO - INIT ");
		driver.get(urlLoggin);
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		String fotocap = "01_INGRESO_AL_APLICATIVO";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* DESPLEGAR CATEGORIA */
		driver.findElement(categoria).click();
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		fotocap = "02_DESPLEGAR_CATEGORIA";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* SELECCIONAR TECNOLOGIA */
		driver.findElement(tecnologia).click();
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		fotocap = "03_SELECCIONAR_TECNOLOGIA";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* SELECCIONAR CONSOLAS */
		driver.findElement(consola).click();
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		fotocap = "04_SELECCIONAR_CONSOLAS";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* DESPLIEGA MARCA */
		driver.findElement(despleMarca).click();
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		fotocap = "05_SELECCIONAR_MARCA";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* SELECCIONAR MARCA */
		driver.findElement(marca).click();
		waitSleep(7);
		fotocap = "06_SELECCIONAR_PRODUCTO";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		
		/* VER DETALLE DEL PRODUCTO */
		driver.findElement(detProd).click();
		fotocap = "07_DETALLE_PRODUCTO";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");
		waitSleep(4);
		
		/* AGREGAR 3 PRODUCTOS */
	    driver.findElement(agregarProd).click();
	    driver.findElement(agregarProd).click();
		AbsTest.waitSleep(DatosSistema.TIME_OUT);
		fotocap = "08_AGREGAR_PROD";
		capturarPantalla(driver, fotocap, "VALIDACIONES/FLUJO_DE_COMPRA");

		LOGGER.info(" VALIDACION INGRESO - END ");
	}
}
